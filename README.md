# Qwant.py

A Python 3 module to scrape Qwant, a privacy respecting Bing frontend.

## Usage
```
from qwant import QwantSearch

agent = QwantSearch()
agent.websearch("sri racha sauce")
agent.imgsearch("puppy images")
```