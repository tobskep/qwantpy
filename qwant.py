import requests
from bs4 import BeautifulSoup as BSoup
import json

class QwantSearch:
    def __init__(self, engine="lite.qwant.com", endpoint="", queryparameter="q", typeparameter="t", useragent=None):
        # basic data we'll need later
        self.engine = engine
        self.endpoint = endpoint
        self.query = queryparameter
        self.type = typeparameter

        # set up a requests session we'll use
        self.rq = requests.Session()

        # switch to custom user agent if specified, otherwise use Chrome 71
        if useragent != None:
            self.rq.headers.update({"user-agent": useragent})
        else:
            self.rq.headers.update({"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"})

    """
        websearch(query, maxresults)
        Search Qwant for *query*, capping off at *maxresults* results.

        Returns: dict, formatted as: {
            "title": "link title",
            "href": "link url",
            "desc": "link description"
        }
    """
    def websearch(self, query: str, maxresults=15):
        results = []
        pageno = 1
        pleaseStopSpammingQwant = False

        # until we've hit the upper limit,
        while not pleaseStopSpammingQwant:
            # search the specified engine with the specified qstring params in the hopes that we'll
            # find something
            url = f"https://{self.engine}/{self.endpoint}?{self.query}={query}&{self.type}=web&p={pageno}"
            qx = self.rq.get(url)

            # WHY ARE YOU BUYING SEARCH RESULTS AT THE SOUP STORE??
            soupstore = BSoup(qx.text, features="lxml")

            # get every result on the page
            for result in soupstore.select("article"):
                if len(results) == maxresults:
                    pleaseStopSpammingQwant = True
                    break

                result_json = {}
                try:
                    # sift through links until we find the external one
                    link = result.find("a")
                    result_json["title"] = link.get_text().strip()
                    
                    # if it's an ad link, ignore it
                    if link["target"] == "_blank": continue
                    
                    # get the description from the result
                    result_json["desc"] = result.select_one("p.desc").text
                    
                    # this URL is stuck in qwant redirect format! let's fix it
                    result_json["href"] = link["href"].strip()
                    result_json["href"] = self.rq.get("https://lite.qwant.com"+result_json["href"], allow_redirects=False).headers["location"]

                    # add json to results... weird, huh?
                    results.append(result_json)

                except KeyError as x:
                    print(x)
                    # ignore the error if the class isn't specified
                    continue
            pageno += 1

            # finally, return the array of search results we found
        return results

    """
        imgsearch(query, maxresults)
        Search Qwant for images matching *query*, capping off at
        *maxresults* results.

        Returns: dict, formatted as: {
            "title": "link title",
            "href": "link url",
            "desc": "link description"
        }
    """
    def imgsearch(self, query: str, maxresults=15):
        results = []
        pageno = 1
        pleaseStopSpammingQwant = False

        # until we've hit the upper limit,
        while not pleaseStopSpammingQwant:
            # search the specified engine with the specified qstring params in the hopes that we'll
            # find something
            url = f"https://{self.engine}/{self.endpoint}?{self.query}={query}&{self.type}=images&p={pageno}"
            qx = self.rq.get(url)

            soupstore = BSoup(qx.text)
            imagebox = soupstore.select_one(".images-container")

            for result in imagebox.select("a"):
                # stop requesting things if we've requested enough
                if len(results) == maxresults:
                    pleaseStopSpammingQwant = True
                    break

                # there's really not much we can do here
                result_json = {}
                result_json["href"] = result["href"]

                # get image source and alt text
                image = result.find("img")
                result_json["thumbnail"] = image["src"]
                result_json["desc"] = image["alt"]

                # de-qwantify the qwant redirect
                result_json["href"] = self.rq.get("https://lite.qwant.com"+result_json["href"], allow_redirects=False).headers["location"]

                # add to results array
                results.append(result_json)
        
        return results
    
if __name__ == "__main__":
    que = input(">")
    Agent = QwantSearch()
    print(Agent.websearch(que))